<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = file_get_contents('./database/seeders/category.json');
        $categories = json_decode($categories, true);
        foreach ($categories as $category) {
            $this->createCategory($category);
        }
    }

    public function createCategory($data, $parent = null)
    {
        // 家电
        $category = new Category(['name'=>$data['name']]);

        $category->is_directory = isset($data['children']);
        if (!is_null($parent)) {
            $category->parent()->associate($parent);
        }

        $category->save();

        if (isset($data['children'])) {
            foreach ($data['children'] as $child) {
                // 电视
                $this->createCategory($child, $category);
            }
        }
    }
}
