<?php

use App\Http\Controllers\Api\AttributeController;
use App\Http\Controllers\Api\CaptchaController;
use App\Http\Controllers\Api\TestController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\CategoriesController;
use App\Http\Controllers\Api\ValuesController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('test', [TestController::class, 'test']);
//Route::post('test2', [TestController::class, 'verifySmsCode']);
// 用户非登录操作
Route::group([],function () {
    Route::prefix('user')->group(function (){
        Route::post('sendSms', [UsersController::class, 'sendSms']);
        Route::post('', [UsersController::class, 'store']);
        Route::post('login', [UsersController::class, 'login']);
    });

    Route::prefix('category')->group(function (){
        Route::get('', [CategoriesController::class, 'index']);
        Route::post('', [CategoriesController::class, 'store']);
        Route::patch('{category}', [CategoriesController::class, 'update']);
        // 新增分类属性
        Route::post('{category}/attribute', [AttributeController::class, 'store']);

    });

    Route::prefix('attribute')->group(function () {
        // 修改分类属性
        Route::patch('{attribute}', [AttributeController::class, 'update']);
        // 添加分类属性
        Route::post('{attribute}/value', [ValuesController::class, 'store']);
    });
    Route::post('captcha', [CaptchaController::class, 'store']);
    Route::post('verify', [CaptchaController::class, 'verify']);

});
// 用户登录后的操作
Route::group(['middleware'=>'token.canRefresh'],function () {
    Route::prefix('user')->group(function (){
        // 登出操作
        Route::post('logout', [UsersController::class, 'logout']);
        // 获取当前用户信息
        Route::post('me', [UsersController::class, 'me']);
    });

});
