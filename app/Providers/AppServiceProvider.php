<?php

namespace App\Providers;

use App\Contract\Captcha\CaptchaContract;
use App\Service\Captcha\MewsCaptchaService;
use App\Service\Sms\EasySmsService;
use Illuminate\Support\ServiceProvider;
use Overtrue\EasySms\EasySms;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CaptchaContract::class, MewsCaptchaService::class);
        $this->app->singleton('captchaService', MewsCaptchaService::class);
        $this->app->singleton('easySms', function () {
            return new EasySms(config('easySms'));
        });
        $this->app->singleton('smsService', EasySmsService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
