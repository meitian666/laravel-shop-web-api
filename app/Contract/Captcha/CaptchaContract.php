<?php

namespace App\Contract\Captcha;

interface CaptchaContract
{
    /**
     * 创建验证码
     * @return mixed
     */
    public function create($phone);

    public function verify($code, $phone, $key);
}
