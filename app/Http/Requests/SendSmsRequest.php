<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendSmsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'  =>  ['required'],
            'phone' =>  [
                'required',
                'phone:CN,mobile',
                'unique:users'
            ],
            'key'   =>  [
                'required',
                'regex:/^(Captcha-)/'
            ]
        ];
    }

    public function messages()
    {
        return [
            'code.required'  => '请输入图形验证码',
            'phone.required'    =>  '请输入手机号',
            'phone.phone'    =>  '手机号码格式错误',
            'phone.unique'    =>  '该手机号已被使用',
            'key.required'  =>  '请输入key值',
            'key.regex'  =>  'key值格式不正确',

        ];
    }
}
