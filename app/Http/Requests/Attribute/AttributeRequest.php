<?php

namespace App\Http\Requests\Attribute;

use Illuminate\Foundation\Http\FormRequest;

class AttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name'  =>  [
                        'required',
                        'between:2,8'
                    ],
                    'order' =>  [
                        'integer',
                        'min:0'
                    ]
                ];
                break;
            case 'PATCH':
                return [
                    'name'  =>  [
                        'between:2,8'
                    ],
                    'order' =>  [
                        'integer',
                        'min:0'
                    ]
                ];
                break;
        }

    }

    public function messages()
    {
        return [
            'name.*'    =>  '请输入2-8位的分类名称',
            'order.*'  =>  '请输入排序数字',
        ];
    }
}
