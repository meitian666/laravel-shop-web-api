<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>   [
                'required',
                'between:2,10',
                'unique:users'
            ],
            'phone' =>  [
                'required',
                'phone:CN,mobile',
                'unique:users'
            ],
            'password'  =>  [
                'required',
                'alpha_num',
                'min:6'
            ],
            'code'  =>  ['required'],
            'key'   =>  [
                'required',
                'regex:/^(Sms-)/'
            ]
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>  '请输入用户名',
            'name.unique'   =>  '该用户名已被使用',
            'name.between'  =>  '用户名在2-10位之间',
            'password.*'    =>  '请输入至少6位的密码,仅限数字和字母',
            'code.*'  =>  '请输入短信验证码',
            'phone.required' =>  '请输入手机号',
            'phone.phone'   =>  '手机号格式错误',
            'phone.unique'  =>  '该手机号已被使用',
            'key.*'   =>  'key值错误'
        ];
    }
}
