<?php

namespace App\Http\Controllers\Api;

use App\Contract\Captcha\CaptchaContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckCaptchaRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CaptchaController extends Controller
{
    /**
     * 创建验证码
     * @param Request $request
     * @param CaptchaContract $captchaContract
     * @return JsonResponse
     */
    public function store(Request $request, CaptchaContract $captchaContract)
    {
        $captcha = $captchaContract->create($request->input('phone'));
        return response()->json(
            $captcha
        );
    }

    /**
     * 验证验证码
     * @param Request $request
     * @return JsonResponse
     */
    public function verify(CheckCaptchaRequest $request, CaptchaContract $captchaContract)
    {
        $code = $request->input('code');
        $phone = $request->input('phone');
        $key = $request->input('key');
        $result= $captchaContract->verify($code, $phone, $key);

        return response()->json([
            'msg'   =>  $result['msg']
        ],$result['code']);

    }
}
