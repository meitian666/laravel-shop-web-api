<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SendSmsRequest;
use App\Http\Requests\User\UserRequest;
use App\Service\User\UserService;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * 用户发送短信
     * @param SendSmsRequest $request
     * @param UserService $userService
     * @return string[]
     */
    public function sendSms(SendSmsRequest $request, UserService $userService)
    {
//        $code = $request->input('code');
//        $phone = $request->input('phone');
//        $key = $request->input('key');
        return $userService->sendSms($request);
    }

    /**
     * 新增用户|用户注册
     * @param UserRequest $request
     * @param UserService $userService
     * @return mixed
     */
    public function store(UserRequest $request, UserService $userService)
    {
        $token = $userService->store($request);
        return response()->json([
            'msg'   =>  '用户注册成功'
        ])->header('Authorization', 'Bearer '.$token);
    }

    /**
     * 登录
     * @param Request $request
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request, UserService $userService)
    {
        $result = $userService->login($request);
        if ($result['code'] != 200) {
            return response()->json([
                'msg'   =>  $result['msg']
            ], $result['code']);
        }
        return response()->json([
            'msg'   =>  '登录成功'
        ])->header('Authorization', 'Bearer '.$result['token']);
    }

    /**
     * 用户登出
     * @param UserService $userService
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(UserService $userService)
    {
        $result = $userService->logout();
        return response()->json([
            'msg'   =>  $result['msg'],
        ], $result['code']);
    }
    /**
     * 获取用户信息
     */
    public function me(UserService $userService)
    {
        $result = $userService->me();
        return response()->json([
            'msg'   =>  $result['msg'],
            'user'  =>  $result['user']
        ], $result['code']);
    }
}
