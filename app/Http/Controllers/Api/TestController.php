<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Service\Captcha\MewsCaptchaService;
use App\Service\Category\CategoryService;
use App\Service\Sms\EasySmsService;
use App\Service\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Mews\Captcha\Facades\Captcha;
use Overtrue\EasySms\EasySms;

class TestController extends Controller
{
    public function test(CategoryService $service)
    {
        return $service->tree();
    }
}
