<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryRequest;
use App\Models\Category;
use App\Service\Category\CategoryService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(CategoryService $categoryService)
    {
        return response()->json([
            'msg'   =>  '分类列表',
            'data'  =>  [
//                'categories'    =>  $categoryService->tree()
                'categories'    =>  $categoryService->getCategories()
            ]
        ]);
    }

    public function store(CategoryRequest $request, CategoryService $categoryService)
    {
        $result = $categoryService->store($request);
        return response()->json([
            'msg'   =>  $result['msg'],
            'data'  =>  $result['data'],
        ], $result['code']);
    }

    public function update(Request $request, Category $category, CategoryService $categoryService)
    {
        $result = $categoryService->update($request, $category);
        return response()->json([
            'msg'   =>  $result['msg'],
            'data'  =>  $result['data'],
        ], $result['code']);
    }

    public function delete(Category $category,CategoryService $categoryService)
    {
        $categoryService->refreshCategories();
        $category->delete();
    }
}
