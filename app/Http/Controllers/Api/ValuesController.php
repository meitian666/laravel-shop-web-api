<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Attribute\AttributeRequest;
use App\Http\Requests\Value\ValueRequest;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Value;
use App\Service\Attribute\AttributeService;
use App\Service\Value\ValueService;
use Illuminate\Http\Request;

class ValuesController extends Controller
{
    /**
     * 新增分类属性
     * @param ValueRequest $request
     * @param Attribute $attribute
     * @param ValueService $valueService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ValueRequest $request, Attribute $attribute, ValueService $valueService)
    {
        $result = $valueService->store($request, $attribute);
        return response()->json([
            'msg'   =>  $result['msg']
        ], $result['code']);
    }

    public function update(ValueRequest $request, Value $value, ValueService $valueService)
    {
        $result = $valueService->update($request, $value);
        return response()->json([
            'msg'   =>  $result['msg'],
            'data'  =>  $result['data']
        ], $result['code']);
    }
}
