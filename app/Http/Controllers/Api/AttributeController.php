<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Attribute\AttributeRequest;
use App\Models\Attribute;
use App\Models\Category;
use App\Service\Attribute\AttributeService;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    /**
     * 新增分类属性
     * @param AttributeRequest $request
     * @param Category $category
     * @param AttributeService $attributeService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AttributeRequest $request, Category $category, AttributeService $attributeService)
    {
        $result = $attributeService->store($request, $category);
        return response()->json([
            'msg'   =>  $result['msg']
        ], $result['code']);
    }

    public function update(AttributeRequest $request, Attribute $attribute, AttributeService $attributeService)
    {
        $result = $attributeService->update($request, $attribute);
        return response()->json([
            'msg'   =>  $result['msg'],
            'data'  =>  $result['data']
        ], $result['code']);
    }
}
