<?php

namespace App\Service\Sms;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Overtrue\EasySms\EasySms;
use Overtrue\EasySms\Exceptions\NoGatewayAvailableException;

class EasySmsService
{
    /**
     * 发送短信
     * @param $phone
     * @return string[]
     * @throws \Exception
     */
    public function send($phone)
    {
        $easySms = app('easySms');
        if (!app()->environment('production')) {
            $code = 1234;
        } else {
            $code = str_pad(random_int(0,9999), 4, 0,STR_PAD_LEFT);
            try {
                $result = $easySms->send($phone, [
                    'template' => 'SMS_69190745',
                    'data' => [
                        'code' => $code
                    ],
                ]);
            } catch (NoGatewayAvailableException $exception) {
                $message = $exception->getException('aliyun')->getMessage();
                abort(400, $message?:'发送短信失败');
            }
        }

        $key = 'Sms-'.Str::random(10);
        Cache::put($key,[
            'phone' =>  $phone,
            'code'  =>  $code
        ], now()->addMinutes(5));
        return [
            'key'   =>  $key,
        ];
    }

    /**
     * 验证短信验证码
     * @param $code
     * @param $phone
     * @param $key
     * @return array
     */
    public function verify($code, $phone, $key)
    {
        if (!$smsCache = Cache::get($key)) {
            return [
                'code'  =>  400,
                'msg'   =>  '验证码未发送或失效'
            ];
        }

        if ($smsCache['code'] != $code || $smsCache['phone'] != $phone) {
            Cache::forget($key);
            return [
                'code'  =>  400,
                'msg'   =>  '验证码错误'
            ];
        }
        Cache::forget($key);
        return [
            'code'  =>  200,
            'msg'   =>  '验证码通过'
        ];
    }
}
