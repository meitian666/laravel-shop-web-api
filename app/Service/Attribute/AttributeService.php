<?php

namespace App\Service\Attribute;

use App\Exceptions\AttributeException;
use App\Http\Requests\Attribute\AttributeRequest;
use App\Models\Attribute;
use App\Models\Category;

class AttributeService
{
    /**
     * 添加分类属性
     * @param AttributeRequest $request
     * @param Category $category
     * @return array
     */
    public function store($request, $category)
    {
        $name = $request->input('name');
        // 判断分类是否为叶子节点
        if($category->is_directory != false) {
            return [
                'msg'   =>  '分类不可添加',
                'code'  =>  422
            ];
        }

        // 判断name和category_id的复合唯一索引
        if (Attribute::query()->where([
            ['name', $name],['category_id', $category->id]
        ])->first()) {
            return [
                'msg'   =>  '分类属性已存在',
                'code'  =>  422
            ];
        }
        $attribute = new Attribute([
            'name'  =>  $name
        ]);

        $category->attributes()->save($attribute);

        return [
            'msg'   =>  '添加分类属性成功',
            'code'  =>  200
        ];
    }

    /**
     * 修改分类属性
     * @param $request
     * @param Attribute $attribute
     * @return array
     * @throws AttributeException
     */

    public function update($request,$attribute)
    {
        $request = $request->only(['name', 'sort']);
        // 判断name和category_id的复合唯一索引
        if (isset($request['name'])&&!$this->canUpdateName($request['name'], $attribute)) {
            throw new AttributeException('分类属性已存在或非法', 422);
        }
//        if ($name&&$name != $attribute->name) {
//            if (Attribute::query()->where([
//                ['name', $name],['category_id', $attribute->category_id]
//            ])->first()) {
//                return [
//                    'msg'   =>  '分类属性已存在',
//                    'code'  =>  422,
//                    'data'  =>  []
//                ];
//            }
//        }
        $attribute->update($request);
        return [
            'msg'   =>  '分类属性修改成功',
            'code'  =>  200,
            'data'  =>  [
                'attribute' =>  $attribute
            ]
        ];
    }

    private function canUpdateName($name, Attribute $attribute)
    {
        return Attribute::query()->where([
            ['name', $name],
            ['category_id', $attribute->category_id],
            ['id', '<>', $attribute->id]
        ])->doesntExist();
    }
}
