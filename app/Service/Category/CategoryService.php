<?php

namespace App\Service\Category;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Redis;

class CategoryService
{
    public function tree($categories = null, $parent_id = null)
    {
        /*----------------递归方法------------------*/
//        if (is_null($categories)) {
//            $categories = Category::all();
//        }

//        return $categories->where('parent_id', $parent_id)->map(function ($category) use ($categories){
//            $data['id'] = $category->id;
//            $data['name'] = $category->name;
//            $data['level'] = $category->level;
//            if ($category->is_directory) {
//                $data['children'] = $this->tree($categories, $category->id);
//            }
//
//            return $data;
//
//        })->values();

//        $tree = [];
//        foreach ($categories as $category) {
//            if ($category->parent_id == $parent_id) {
//                $data['id'] = $category->id;
//                $data['name'] = $category->name;
//
//                if ($category->is_directory) {
//                    $data['children'] = $this->tree($categories, $category->id);
//                }
//
//                $tree[] = $data;
//            }
//        }

//        return $tree;
        /*-----------------引用方法----------------------*/
        if (is_null($categories)) {
            $categories = Category::all();
        }
        $tree = [];
        $list = [];

        foreach ($categories->toArray() as $category) {
            $list[$category['id']] = Arr::only($category, ['id', 'name', 'parent_id', 'level', 'is_directory', 'path']);

            if ($list[$category['id']]['is_directory'] == 1) {
                $list[$category['id']]['children'] = [];
            }
        }

        foreach ($list as $value) {
            if ($value['parent_id'] == $parent_id) {
                $tree[] = &$list[$value['id']];
            } else {
                $list[$value['parent_id']]['children'][] = &$list[$value['id']];
            }
        }
        return $tree;
    }

    /**
     * 新增分类
     * @param $request
     * @return array
     */
    public function store($request)
    {
        // 直接创建
//        $category = Category::create([
//            'name'  =>  $request->input('name'),
//            'parent_id' =>  $request->input('parent_id'),
//            'is_directory'  =>  $request->input('is_directory')
//        ]);
        // TODO
        //  1:parent_id是否存在
        //  2:parent_id对应的父类是否存在,并且is_directory为1
        $category = new Category([
            'name'  =>  $request->input('name'),
            'is_directory'  =>  $request->input('is_directory')
        ]);
        if (!is_null($request->input('parent_id'))) {
            if (!$parent = Category::find($request->input('parent_id'))) {
                return [
                    'msg'   =>  '父级分类不存在',
                    'code'  =>  '422',
                    'data'  =>  []
                ];
            }

            if ($parent->is_directory != 1) {
                return [
                    'msg'   =>  '父级分类不可添加',
                    'code'  =>  '422',
                    'data'  =>  []
                ];
            }

            $category->parent()->associate($parent);

        }
        $category->save();
        $this->refreshCategories();
        return [
            'msg'   =>  '添加分类成功',
            'code'  =>  200,
            'data'  =>  [
                'category'  =>  new CategoryResource($category)
            ]
        ];
    }
    /**
     * 修改分类
     */
    public function update($request, Category $category)
    {
        // 如果有is_directory字段
        if ($request->has('is_directory') && in_array($request->input('is_directory'), [0,1])) {
            // 判断能否修改is_directory字段,即是否有children
            if ($this->hasChildren($category)) {
                return [
                    'msg'   =>  '是否有子分类属性不能改变',
                    'code'  =>  '422',
                    'data'  =>  []
                ];
            }
            $request = $request->only(['name', 'is_directory']);
        } else {
            $request = $request->only(['name']);
        }
        $category->update($request);
        $this->refreshCategories();
        return [
            'msg'   =>  '修改分类成功',
            'code'  =>  200,
            'data'  =>  [
                'category'  =>  new CategoryResource($category)
            ]
        ];
    }

    public function getCategories()
    {
        if (is_null(Redis::get('categories'))) {
            $this->refreshCategories();
        }

        return unserialize(Redis::get('categories'));

    }



    public function hasChildren(Category $category)
    {
        return $category->where('path', 'like', $category->path.$category->id.'-%')->first();
    }

    public function refreshCategories()
    {
        Redis::set('categories', serialize($this->tree()));
    }
}
