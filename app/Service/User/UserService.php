<?php

namespace App\Service\User;


use App\Events\UserRegister;
use App\Http\Requests\User\UserRequest;
use App\Models\User;
use App\Service\Sms\EasySmsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserService
{

    /**
     * @param $request
     * @return string[]
     */
    public function sendSms($request)
    {
        // 验证图形验证码是否正确
        $code = $request->input('code');
        $phone = $request->input('phone');
        $key = $request->input('key');
        $result= app('captchaService')->verify($code, $phone, $key);
        if ($result['code'] != 200) {
            abort($result['code'], $result['msg']);
        }

        $key = app('smsService')->send($phone);
        return $key;
    }

    /**
     * 验证短信验证码
     * @param $request
     * @return array
     */
    public function verify($request)
    {
        $code = $request->input('code');
        $phone = $request->input('phone');
        $key = $request->input('key');
        $result = app('smsService')->verify($code, $phone, $key);
        if ($result['code'] != 200) {
            abort($result['code'], $result['msg']);
        }
        return $result;
    }

    /**
     * 新增用户
     * @param UserRequest $request
     * @return mixed
     */
    public function store($request)
    {
//        $this->verify($request);// 验证短信验证码
        $user = User::create([
            'name'  =>  $request->input('name'),
            'phone' =>  $request->input('phone'),
            'password'  =>  Hash::make($request->input('password'))
        ]);
        event(new UserRegister($user));
        return auth()->login($user);
    }

    /**
     * 用户登录
     * @param $request
     * @return array
     */
    public function login($request)
    {
        $credentials = $request->only(['phone', 'password']);

        // 提示用户不存在
        if(!$user = User::query()->where(['phone'=>$request->input('phone')])->first()) {
            return [
                'code'  =>  422,
                'msg'   =>  '用户不存在'
            ];
        }
        if (! $token = auth()->attempt($credentials)) {
            return [
                'code'  =>  422,
                'msg'   =>  '手机号或密码错误'
            ];
        }

        return [
            'code'  =>  200,
            'msg'   =>  '登录成功',
            'token' =>  $token
        ];
    }

    /**
     * 用户登出
     *
     */
    public function logout()
    {
        try {
            auth()->logout();
            return [
                'msg'   =>  '用户登出',
                'code'  =>  200,
            ];
        } catch (JWTException $exception) {
            return [
                'code'  =>  400,
                'msg'   =>  $exception->getMessage(),
                'user'  =>  []
            ];
        }
    }
    /**
     * 用户信息
     * @return array
     */
    public function me()
    {
        try {
            $user = auth()->user();
            return [
                'msg'   =>  '获取用户信息成功',
                'code'  =>  200,
                'user'  =>  $user
            ];
        } catch (JWTException $exception) {
            return [
                'code'  =>  400,
                'msg'   =>  $exception->getMessage(),
                'user'  =>  []
            ];
        }
    }
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
