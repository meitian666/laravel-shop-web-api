<?php

namespace App\Service\Value;

use App\Http\Requests\Attribute\AttributeRequest;
use App\Http\Requests\Value\ValueRequest;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Value;

class ValueService
{
    /**
     * 添加分类属性
     * @param ValueRequest $request
     * @param Attribute $attribute
     * @return array
     */
    public function store(ValueRequest $request, Attribute $attribute)
    {
        $name = $request->input('name');
        // 判断name和attribute_id的复合唯一索引
        if (Value::query()->where([
            ['name', $name],['attribute_id', $attribute->id]
        ])->first()) {
            return [
                'msg'   =>  '分类属性值已存在',
                'code'  =>  422
            ];
        }
        $value = new Value([
            'name'  =>  $name
        ]);

        $attribute->values()->save($value);

        return [
            'msg'   =>  '添加分类属性值成功',
            'code'  =>  200
        ];
    }

    /**
     * 修改分类属性
     * @param ValueRequest $request
     * @param Attribute $attribute
     * @return array
     */

    public function update(ValueRequest $request, Value $value)
    {
        $request = $request->only(['name', 'sort']);
        // 判断name和category_id的复合唯一索引
        $name = $request['name'];
        if ($name&&$name != $value->name) {
            if (Attribute::query()->where([
                ['name', $name],['attribute_id', $value->attribute_id]
            ])->first()) {
                return [
                    'msg'   =>  '分类属性值已存在',
                    'code'  =>  422,
                    'data'  =>  []
                ];
            }
        }
        $value->update($request);
        return [
            'msg'   =>  '分类属性修改成功',
            'code'  =>  200,
            'data'  =>  [
                'attribute' =>  $value
            ]
        ];
    }
}
