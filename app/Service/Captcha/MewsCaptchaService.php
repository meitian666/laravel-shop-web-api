<?php

namespace App\Service\Captcha;

use App\Contract\Captcha\CaptchaContract;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Mews\Captcha\Facades\Captcha;

class MewsCaptchaService implements CaptchaContract
{
    /**
     * 创建验证码
     * @return mixed
     */
    public function create($phone)
    {
        $key = 'Captcha-'.Str::random(10);
        $captcha = Captcha::create('default', true);
        Cache::put($key,[
            'phone' =>  $phone,
            'captcha_key'   =>  $captcha['key']
        ]);
        return [
            'key'   =>  $key,
            'img'   =>  $captcha['img']
        ];
    }

    public function verify($code, $phone, $key)
    {
        // 验证缓存
        if (!$captchaCache = Cache::get($key)) {
            return [
                'code'  =>  422,
                'msg'   =>  '请求的验证信息不存在'
            ];
        }
        // 验证手机号
        if ($captchaCache['phone'] != $phone) {
            Cache::forget($key);
            return [
                'code'  =>  422,
                'msg'   =>  '手机号信息不正确'
            ];
        }
        // 验证验证码
        if (!captcha_api_check($code,$captchaCache['captcha_key'])) {
            Cache::forget($key);
            return [
                'code'  =>  422,
                'msg'   =>  '验证码不正确'
            ];
        }

        return [
            'code'  =>  200,
            'msg'   =>  '验证通过'
        ];
    }
}
