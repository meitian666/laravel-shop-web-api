<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;

class MakeService extends GeneratorCommand
{
    protected $name = 'make:service';
    protected $type = 'Service';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建一个service';


    protected function getStub()
    {
        return resource_path('/stubs/support.stub');
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Service';
    }
}
