<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class MakeSupport extends GeneratorCommand
{
    protected $name = 'make:support';
    protected $type = 'Support';

    protected $description = '创建一个support';


    protected function getStub()
    {
        return resource_path('/stubs/support.stub');
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Support';
    }
}
