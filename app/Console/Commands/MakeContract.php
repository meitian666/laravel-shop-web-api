<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;

class MakeContract extends GeneratorCommand
{
    protected $name = 'make:contract';
    protected $type = 'Contract';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建一个Contract';


    protected function getStub()
    {
        return resource_path('/stubs/contract.stub');
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Contract';
    }
}
