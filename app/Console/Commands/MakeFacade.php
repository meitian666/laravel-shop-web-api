<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;

class MakeFacade extends GeneratorCommand
{
    protected $name = 'make:facade';
    protected $type = 'Facade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '创建一个Facade';


    protected function getStub()
    {
        return resource_path('/stubs/support.stub');
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Facade';
    }
}
