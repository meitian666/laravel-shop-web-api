<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'category_id', 'on_sale',
        'rating', 'sales_count', 'remark_count',
        'price'
    ];

    protected $casts = [
        'on_sale'   =>  'boolean'
    ];

    /**
     * 与分类的关联关系
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    /**
     * 与商品SKU的一对多关联
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function skus()
    {
        return $this->hasMany(ProductSku::class);
    }

    /**
     * 与商品图片的一对多关联
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
