<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSku extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'price', 'stock',
        'product_id', 'on_sale'
    ];

    protected $casts = [
        'on_sale'   =>  'boolean'
    ];

    /**
     * 与商品的一对多关联
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
