<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'parent_id', 'is_directory', 'level', 'path'
    ];

    protected $casts = [
        'is_directory'  =>  'boolean'
    ];



    protected static function boot()
    {
        parent::boot();

        static::creating(function ($category) {
            // 是否是顶级分类
            if (is_null($category->parent_id)) {
                $category->level = 1;
                $category->path = '-';
            }
            else {
                $category->level = $category->parent->level + 1;
                $category->path = $category->parent->path.$category->parent_id . '-';
            }

        });
    }



    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * 与商品的关联关系
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    /**
     * 与分类属性的关联关系
     */
    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }
}
