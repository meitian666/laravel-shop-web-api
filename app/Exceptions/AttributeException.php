<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class AttributeException extends Exception
{
    public function __construct($message = "", $code = 0)
    {
        parent::__construct($message, $code);
    }

    public function render()
    {
        return response()->json([
            'msg'   =>  $this->message
        ], $this->code);
    }
}
